import React from 'react'
import {
	Router,
	Route,
	IndexRoute,
	Redirect,
	browserHistory
} from 'react-router'
import App from 'container/app'
import Home from 'container/Home'
import About from 'container/About'

export default (store, history) => (
	<Router history={history}>
		<Route path='/' component={App}>
			<IndexRoute component={Home} />
			<Route path='about' component={About} />
		</Route>
	</Router>
)
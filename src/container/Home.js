import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router' 
import actions from 'actions'


const { getUsers } = actions

class Home extends Component {

	state = {
		x: 0
	}

	componentWillMount(){
		this.props.getUsers()
	}

	eiei(){
		this.setState({x: this.state.x +1})
	}

	render(){
		return (
			<div>
				<h1>Home Sweet Home</h1>
				<br />
				<code>{JSON.stringify(this.props.users, null, 2)}</code>
				<br />
				{this.state.x}
				<button onClick={() => this.eiei()}>Click</button>
				<br />
				<Link to='/about'>About</Link>

				<br />
				
				<button type="button" class="btn btn-default navbar-btn">Test</button>
				<nav class="navbar navbar-default navbar-static-top">
  					<div class="container">
    					...Test
  					</div>
				</nav>

			</div>
		)
	}
}

const mapStateToProps = (state) => ({ //เอา state จาก store มาใส่ product
	users: state.users.get.data
})

const mapDispatchToProps = (dispatch) => ({ // เพื่อให้ส่งค่าให้ reducer แล้วจะได้เก็บค่าลงใน store 
	getUsers() { 
	    dispatch(getUsers())
	}
})

Home = connect(
	mapStateToProps,
	mapDispatchToProps
)(Home)

export default Home